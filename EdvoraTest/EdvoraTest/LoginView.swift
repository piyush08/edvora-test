//
//  ContentView.swift
//  EdvoraTest
//
//  Created by Piyush Agrawal on 17/11/21.
//

import SwiftUI


struct LoginView: View {
        
    @State var username: String = ""
    @State var password: String = ""
    @State var email: String = ""
    
    @State private var showPassword = false
    @State private var showingAlert = false
    
    @StateObject var loginvm = LoginViewModel()
    
    @State var alertMsg = ""
    
    var body: some View {
        
        ScrollView(.vertical, showsIndicators: false) {
        VStack(spacing: 30) {
            
            Image("Logo")
                .resizable()
                .aspectRatio(contentMode: .fit)
                .padding(.vertical, 10)
                .frame(maxHeight:120)
            
            
            VStack(spacing:20){ //TextInput
                HStack {
                    Image(systemName: "person.crop.circle.fill")
                        .foregroundColor(.gray)
                    ZStack(alignment: .leading) {
                        Text("UserName")
                            .font(username.isEmpty ? .title2 : .body)
                            .foregroundColor(.gray)
                            .background(username.isEmpty ? Color("lightGreyColor") : .white)
                            .offset(y: username.isEmpty ? 0 : -28)
                            .scaleEffect(username.isEmpty ? 1 : 0.9)
                            
                        TextField("", text: $username)
                            .frame(height: 50)
                    }
                }
                .animation(.easeOut)
                .padding(.horizontal, 5)
                .background(RoundedRectangle(cornerRadius: 10)
                                .stroke(username.isEmpty ?  .gray :Color("myBrownColor").opacity(0.5), lineWidth:  1))
                .background(Color("lightGreyColor"))
                //.cornerRadius(5.0)
                
                HStack {
                    Image(systemName: "at")
                        .foregroundColor(.gray)
                    ZStack(alignment: .leading) {
                        Text("Email")
                            .font(email.isEmpty ? .title2 : .body)
                            .foregroundColor(.gray)
                            .background(email.isEmpty ? Color("lightGreyColor") : .white)
                            .offset(y: email.isEmpty ? 0 : -28)
                            .scaleEffect(email.isEmpty ? 1 : 0.9)
                            
                        TextField("", text: $email)
                            .frame(height: 50)
                    }
                }
                .animation(.easeOut)
                .padding(.horizontal, 5)
                .background(RoundedRectangle(cornerRadius: 10)
                                .stroke(email.isEmpty ?  .gray : Color("myBrownColor").opacity(0.5), lineWidth: 1))
                .background(Color("lightGreyColor"))
                //.cornerRadius(5.0)
                
                HStack(spacing: 8){
                    
                    Image(systemName: "key.fill")
                        .foregroundColor(.gray)
                    
                    ZStack(alignment: .leading) {
                        Text("Password")
                            .font(password.isEmpty ? .title2 : .body)
                            .foregroundColor(.gray)
                            .background(password.isEmpty ? Color("lightGreyColor") : .white)
                            .offset(y: password.isEmpty ? 0 : -28)
                            .scaleEffect(password.isEmpty ? 1 : 0.9)
                    if self.showPassword {
                        TextField("", text: $password)
                            .frame(height: 50)
                        
                    } else {
                        SecureField("", text: $password)
                            .frame(height: 50)
                        
                    }
                    }
                    Button(action: {
                        self.showPassword.toggle()
                    }){
                        Image(systemName: showPassword ? "eye.slash.fill" : "eye.fill")
                    }.accentColor(.gray)
                }
                .animation(.easeOut)
                .padding(.horizontal, 5)
                .background(RoundedRectangle(cornerRadius: 10)
                                .stroke(password.isEmpty ?  .gray :Color("myBrownColor").opacity(0.5), lineWidth:  1))
                .background(Color("lightGreyColor"))
                //.cornerRadius(10)
                
                
                HStack {
                    Spacer()
                    Button(action: {}) {
                        Text("Forgotten Password?")
                            .foregroundColor(.gray)
                            .font(.footnote)
                    }
                }
            }
            .ignoresSafeArea(.keyboard, edges: .bottom)
            
            Button(action: {
                let result = loginvm.isValidInputs(userName: username, email: email, password: password)
                
                self.alertMsg = result.1
                
                showingAlert.toggle()
            }) {
                Text("Login")
                    .frame(maxWidth:.infinity, minHeight: 50)
                    .background(Color("myBrownColor"))
                    .cornerRadius(10)
                    .foregroundColor(.white)
                    .font(.headline)
            }.alert(isPresented: $showingAlert) {
                Alert(title: Text(alertMsg))
            }
            
            //SignUp Button
            Button(action: {}){
                HStack{
                    Text("Don't have an account?")
                        .foregroundColor(.gray)
                        .font(.footnote)
                    Text("Sign up")
                        .foregroundColor(Color("myBrownColor"))
                }
            }
            
            Spacer()
        }
        .frame(maxWidth:400)
        .padding()
        }
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}


