//
//  LoginViewModel.swift
//  EdvoraTest
//
//  Created by Piyush Agrawal on 18/11/21.
//

import Foundation

class LoginViewModel : ObservableObject {
    
    func isValidInputs(userName:String, email:String, password:String) -> (Bool,String) {
       
        if email.isBlank {
           return (false,"Email can't be blank.")
       } else if !email.isValidEmail {
           return (false,"Email is not valid.")
       }else if userName.isBlank{
           return (false, "UserName can't be blank")
       } else if userName.isContainsUppercase{
           return (false, "Username should not have upper case letter")
       } else if password.isBlank{
           return (false,"Password can't be blank.")
       } else if !(password.isValidPassword) {
           return (false,"Please enter valid password")
       }
       return (true,"success")
   }
}
