//
//  Extensions.swift
//  EdvoraTest
//
//  Created by Piyush Agrawal on 17/11/21.
//

import Foundation

extension String {
    
    var trim: String {
        return self.trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var isBlank: Bool {
        return self.trim.isEmpty
    }
    
    var isValidEmail: Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return predicate.evaluate(with:self)
    }
    var isValidPassword: Bool {
        //let passwordRegex = "^(?=.*[a-z])(?=.*[@$!%*#?&])[0-9a-zA-Z@$!%*#?&]{8,}"
        let passwordRegex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,}"
        let predicate = NSPredicate(format:"SELF MATCHES %@", passwordRegex)
        return predicate.evaluate(with:self)
    }
    
    var isContainsUppercase : Bool {
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let predicate = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        return predicate.evaluate(with:self)
    }
}
