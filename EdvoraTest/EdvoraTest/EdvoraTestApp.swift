//
//  EdvoraTestApp.swift
//  EdvoraTest
//
//  Created by Piyush Agrawal on 17/11/21.
//

import SwiftUI

@main
struct EdvoraTestApp: App {
    var body: some Scene {
        WindowGroup {
            LoginView()
        }
    }
}
